<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Wainwright\CasinoDogOperatorApi\Controllers\CasinoDogCallbackController;
use Wainwright\CasinoDogOperatorApi\Controllers\CasinoDogCreateSessionController;
use Wainwright\CasinoDogOperatorApi\Controllers\APIController;

Route::middleware('api', 'throttle:15,1')->prefix('api/casino-dog-operator-api/')->group(function () {
        Route::get('/createSession', [CasinoDogCreateSessionController::class, 'test_create']);
});

Route::middleware('api', 'throttle:5000,1')->prefix('api/casino-dog-operator-api/')->group(function () {
        Route::get('/callback', [CasinoDogCallbackController::class, 'callback']);
	Route::get('/gameslist/{layout}', [APIController::class, 'gameslistEndpoint']);
});

Route::middleware('api', 'throttle:5000,1')->any('settings', function (Request $request) {
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
	return 'hello';
});


Route::middleware('api', 'throttle:5000,1')->prefix('api/')->group(function () {
        Route::get('/games', [APIController::class, 'games']);
	Route::get('/games/{slug}', [APIController::class, 'games_info']);
	Route::get('/tags', [APIController::class, 'tags']);
        Route::get('/categories', [APIController::class, 'categories']);
	Route::get('/play/{slug}', [APIController::class, 'play']);
});



Route::middleware('api', 'throttle:5000,1')->any('/games', function (Request $request) {
    $page = $request->page ? $request->page : 1;
    $limit = $request->limit ? $request->limit : 25;

    $cache = Cache::get($request->fullUrl());
    if($cache) {
        return json_decode($cache);
    }

    if($request->search) {
        $provider_explode = explode('categories.slug:', $request->search);
        if(isset($provider_explode[1])) {
            $explode = explode(';', $provider_explode[1]);
            $provider = $explode[0];
	    $http = Http::get('http://jacob-7-1277cp.mooo.com/api/casino-dog-operator-api/gameslist/wainwright_casino?page='.$page.'&limit='.$limit.'&provider='.$provider);
            //$http = Http::get('http://jacob-7-1277cp.mooo.com/api/casino-dog-operator-api/gameslist/wainwright_casino?page='.$page.'&limit='.$limit.'&provider='.$provider);
            $cache = Cache::set($request->fullUrl(), $cache, 300);
            return json_decode($http);
        }
    }

    $bonus = 0;
    $jackpot = 0;
    if($request->searchJoin) {
        if(str_contains('tags.slug:bonusbuy', $request->searchJoin)) {
            $bonus = 1;
        }
        if(str_contains('tags.slug:jackpot', $request->searchJoin)) {
            $jackpot = 1;
        }
        $http = Http::get('http://jacob-7-1277cp.mooo.com/api/casino-dog-operator-api/gameslist/wainwright_casino?page='.$page.'&limit='.$limit.'&bonus='.$bonus.'&jackpot='.$jackpot);
        $cache = Cache::set($request->fullUrl(), $cache, 300);
        return json_decode($http);
    }


    $http = Http::get('http://jacob-7-1277cp.mooo.com/api/casino-dog-operator-api/gameslist/wainwright_casino?page='.$page.'&limit='.$limit);
    $cache = Cache::set($request->fullUrl(), $cache, 300);

    return json_decode($http);
});
Route::middleware('api', 'throttle:5000,1')->any('/play/localhost/{slug}', function ($slug, Request $request) {
    $ip = $request->ip();
    $format_time_to_hour = Carbon\Carbon::parse(now())->format('H');
    $format_time_to_day = Carbon\Carbon::parse(now())->format('d');
    $player_id = md5($ip.$format_time_to_hour.$format_time_to_day);

    $create_session_apiurl = "https://win.radio.fm/api/createSession?game=".$slug."&player=".$player_id."&currency=USD&operator_key=a7e3d4f46effcfe7dd79127acde85d29&mode=real";
    $create_session_request = Http::get($create_session_apiurl);
    return redirect($create_session_request['message']['session_url']);
});

Route::middleware('api', 'throttle:5000,1')->any('/play/{slug}', function ($slug, Request $request) {
    if($request->ip() === '127.0.0.1') {
        return 'localhost - load /play/localhost/{slug} instead';
    }

    $ip = $request->ip();
    $format_time_to_hour = Carbon\Carbon::parse(now())->format('H');
    $format_time_to_day = Carbon\Carbon::parse(now())->format('d');
    $player_id = md5($ip.$format_time_to_hour.$format_time_to_day);

    $create_session_apiurl = "https://win.radio.fm/api/createSession?game=".$slug."&player=".$player_id."&currency=USD&operator_key=a7e3d4f46effcfe7dd79127acde85d29&mode=real";
    $create_session_request = Http::get($create_session_apiurl);
    return redirect($create_session_request['message']['session_url']);
});


Route::middleware('api', 'throttle:5000,1')->any('/games/{slug}', function ($slug, Request $request) {

    $cached_gameinfo_api = Cache::get('gameinfo-api-'.$slug);
    if(!$cached_gameinfo_api) {
        $http = Http::get('https://win.radio.fm/api/gameinfo/'.$slug);
        $http = json_decode($http, true);
        Cache::set('gameinfo-api-'.$slug, $http, 120);
    } else {
	$http = Cache::get('gameinfo-api-'.$slug);
        $cached_gameinfo_api = Cache::get('gameinfo-api-'.$slug);
    }


    /* Generate a user ID based on IP that resets every hour */
    $ip = $request->ip();
    $format_time_to_hour = Carbon\Carbon::parse(now())->format('H');
    $format_time_to_day = Carbon\Carbon::parse(now())->format('d');
    $player_id = md5($ip.$format_time_to_hour.$format_time_to_day);


    $create_session = env('APP_URL')."/api/play/".$http['slug'];
    $cached_session = Cache::get($player_id.$http['slug']);
    if($cached_session) {
        return $cached_session;
    }

    $orig_tags = $http['tags'];
    $tags = [];
    if(str_contains(json_encode($orig_tags), 'slots')) {
        $data = [
            'id' => 1,
            'name' => 'Slots',
            'slug' => 'slots',
            'icon' => null,
            'image' => [],
            'details' => 'Slotmachine Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }
    if(str_contains(json_encode($orig_tags), 'live')) {
        $data = [
            'id' => 2,
            'name' => 'Live',
            'slug' => 'live',
            'icon' => null,
            'image' => [],
            'details' => 'Live Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'bonusbuy')) {
        $data = [
            'id' => 3,
            'name' => 'Bonus Buy',
            'slug' => 'bonusbuy',
            'icon' => null,
            'image' => [],
            'details' => 'Bonus Buy Feature',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'jackpot')) {
        $data = [
            'id' => 4,
            'name' => 'jackpot',
            'slug' => 'Jackpot',
            'icon' => null,
            'image' => [],
            'details' => 'Jackpot',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'casino')) {
        $data = [
            'id' => 4,
            'name' => 'Casino Table Game',
            'slug' => 'casino',
            'icon' => null,
            'image' => [],
            'details' => 'Casino Table Game',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'live')) {
        $data = [
            'id' => 1,
            'name' => 'Live',
            'slug' => 'live',
            'icon' => null,
            'image' => [],
            'details' => 'Live Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    $data = [
        'id' => $http['id'],
        'name' => $http['name'],
        'slug' => $http['slug'],
        'player_id' => $player_id,
        'real_url' => $create_session,
        'description' => $http['description'],
        'play_real' =>  $create_session,
        'fake_iframe_url' => 'https://google.com',
        'status' => 'active',
        'image' => $http['image'],
        'tags' =>  $tags,
        'type' => $http['type'],
        'provider' => [
            'name' => $http['provider'],
            'slug' => $http['provider'],
            'cover_image' => [
                'original' => $http['image'],
                'thumbnail' => $http['image'],
            ],
            'logo' => [
                'original' => $http['image'],
                'thumbnail' => $http['image'],
            ],
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ],
    ];
    Cache::set($player_id.$http['slug'], $data, 90);

    return json($data);

});

Route::middleware('api', 'throttle:5000,1')->any('/tags ', function (Request $request) {
    $cache = Cache::get('tags-get');
    if($cache) {
        return $cache;
    }
    $http = Http::get('https://win.radio.fm/api/tags');
    Cache::set('tags-get', json_decode($http, true), 90);
    return $http;
});

Route::middleware('api', 'throttle:500,1')->any('/categories', function (Request $request) {
    if($request->page) {
        $cache = Cache::get('category-page-'.$request->page);
        if($cache) {
            return $cache;
        }
        $http = Http::get('https://win.radio.fm/api/providersListEndpoint?scheme_layout=wainwright_casino&page='.$request->page);
        Cache::set('category-page-'.$request->page, $http, 120);
        return $http;
    } else {
        $cache = Cache::get('category-all');
        if($cache) {
            return $cache;
        }
        $http = Http::get('https://win.radio.fm/api/providersListEndpoint?scheme_layout=wainwright_casino');
        Cache::set('category-all', json_decode($http, true), 120);
        return $http;
    }
});
