<?php

namespace Wainwright\CasinoDogOperatorApi\Commands;

use Illuminate\Support\Facades\Http;
use Wainwright\CasinoDogOperatorApi\Models\OperatorGameslist;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use RuntimeException;
use Symfony\Component\Process\Process;

class InstallCasinoDogOperator extends Command
{
    protected $signature = 'casino-dog-operator:install';
    public $description = 'Install casino-dog gameslist.';

    public function handle()
    {
        if($this->confirm('Do you want to run database migrations?')) {
            \Artisan::call('vendor:publish --tag="casino-dog-operator-api-migrations"');
            $this->info('> Running..  "vendor:publish --tag="casino-dog-operator-api-migrations"');
            \Artisan::call('migrate');
            $this->info('> Running..  "artisan migrate"');
        }  else {
            $this->info('.. Skipped database migrations');
        }

        $gameslist_endpoint = config('casino-dog-operator-api.endpoints.gameslist');

	if($this->confirm('Do you want to import DB list from '.$gameslist_endpoint.'?')) {
 	    $http = Http::get(config('casino-dog-operator-api.endpoints.gameslist'));
	    $http = json_decode($http, true);
	    //$http = json_encode($http);
		//dd($http);
            $filteredArray = array_map(function($array) {
               unset($array['id']);
               return $array;
	    }, $http);
	    foreach($filteredArray as $game) {
		$game['realmoney'] = '[]';
		//dd($game);
		OperatorGameslist::insert($game);
	    }
	}
        return self::SUCCESS;
    }

    public function replaceInBetweenInFile($a, $b, $replace, $path)
    {
        $file_get_contents = file_get_contents($path);
        $in_between = $this->in_between($a, $b, $file_get_contents);
        if($in_between) {
            $search_string = stripcslashes($a.$in_between.$b);
            $replace_string = stripcslashes($a.$replace.$b);
            file_put_contents($path, str_replace($search_string, $replace_string, file_get_contents($path)));
            return self::SUCCESS;
        }
        return self::SUCCESS;
    }



    public function in_between($a, $b, $data)
    {
        preg_match('/'.$a.'(.*?)'.$b.'/s', $data, $match);
        if(!isset($match[1])) {
            return false;
        }
        return $match[1];
    }

    protected function requireComposerPackages($packages)
    {
        $composer = $this->option('composer');

        if ($composer !== 'global') {
            $command = ['php', $composer, 'require'];
        }

        $command = array_merge(
            $command ?? ['composer', 'require'],
            is_array($packages) ? $packages : func_get_args()
        );

        (new Process($command, base_path(), ['COMPOSER_MEMORY_LIMIT' => '-1']))
            ->setTimeout(null)
            ->run(function ($type, $output) {
                $this->output->write($output);
            });
    }

}
