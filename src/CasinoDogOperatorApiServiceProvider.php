<?php

namespace Wainwright\CasinoDogOperatorApi;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Wainwright\CasinoDogOperatorApi\Commands\InstallCasinoDogOperator;

class CasinoDogOperatorApiServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('casino-dog-operator-api')
            ->hasConfigFile()
            ->hasViews()
            ->hasRoutes(['api'])
            ->hasMigrations(['create_playerbalances_table', 'create_operator_gameslist_table'])
            ->hasCommand(InstallCasinoDogOperator::class);
            $kernel = app(\Illuminate\Contracts\Http\Kernel::class);
            $kernel->pushMiddleware(\Wainwright\CasinoDogOperatorApi\Middleware\RestrictIpAddressMiddleware::class);
    }
}
