<?php
namespace Wainwright\CasinoDogOperatorApi\Models;
use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class PlayerBalances extends Eloquent  {
    protected $table = 'wainwright_player_balances';
    protected $timestamp = true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'player_id',
        'player_name',
        'currency',
        'balance',
    ];
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function select_player($player_id, $currency)
    {
        $player = self::where('player_id', $player_id)->first();

        if(!$player) {
            $data = [
                'player_id' => $player_id,
                'player_name' => $player_id.'-name',
                'currency' => $currency,
                'balance' => config('casino-dog-operator-api.test_settings.start_balance') ?? 0,
                'created_at' => now(),
                'updated_at' => now(),
            ];
            self::insert($data);
            $player = self::where('player_id', $player_id)->first();
        }
        return $player;
    }

    public function select_player_balance($player_id, $currency)
    {
        $player = $this->select_player($player_id, $currency);
        return (int) $player->balance;
    }


    public function process_game($player_id, $bet, $win, $currency, $gamedata)
    {
        $player = $this->select_player($player_id, $currency);
        $balance = $player->balance;

        if($bet > 0) {
            if($bet > $balance) {
                abort(400, 'Bet bigger then balance');
            }
        }

        $balance = $this->select_player_balance($player_id, $currency);
        $updateBalanceOnBet = (int) $balance - $bet;
        $updateBalanceOnWin = (int) $updateBalanceOnBet + $win;
        $final = PlayerBalances::where('player_id', $player_id)->update(['balance' => $updateBalanceOnWin]);

        return (int) $updateBalanceOnWin;
    }

}