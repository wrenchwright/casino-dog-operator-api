<?php
namespace Wainwright\CasinoDogOperatorApi\Controllers;

use Illuminate\Http\Request;
use Carbon;
use Illuminate\Support\Facades\Http;
//use Illuminate\Http\Client;
use Illuminate\Http\Response;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
//#use Wainwright\CasinoDog\CasinoDog;
//use Wainwright\CasinoDog\Controllers\Game\SessionsHandler;
//use Wainwright\CasinoDog\Controllers\Game\OperatorsController;
use Illuminate\Support\Facades\Log;
use Wainwright\CasinoDogOperatorApi\Traits\ApiResponseHelper;
use Wainwright\CasinoDogOperatorApi\Models\OperatorGameslist;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Pagination\Paginator;
class APIController
{
   use ApiResponseHelper;

    public function __construct()
    {
        $this->operator_key = config('casino-dog-operator-api.access.key');
        $this->operator_secret = config('casino-dog-operator-api.access.secret');
    }

   public function gameslist_ocb($games)
   {
    foreach($games as $game) {
        $explode_game = explode(':', $game->slug);
        if(isset($explode_game[1])) {
            $game_software = $explode_game[0];
            $game_raw = $explode_game[1];
            $img_url = 'https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
        } else {
            $img_url = 'http://5.196.36.36:9000/thumbnail/i-9102777/'.$game->gid.'.png';
        }
        $games_list[] = array(
            'id' => $game->id,
            'api_game_id' => $game->slug,
            'provider' => $game->provider,
            'category_id' => "0",
            'section_id' => "1",
            'brand_id' => "0",
            'name' => $game->name,
            'description' => $game->name,
            'supports' => array(
                'ios' => true,
                'android' => true,
            ),
            'image_file' => 'https://cdn.softswiss.net/i/   /'.$game_software.'/'.$game_raw.'.png',
            'lobby_image_file' => "",
            'position_category' => "0",
            'position_brand' => "0",
            'widescreen' => "0",
            'is_new' => "0",
            'use_jackpot' => "0",
            'use_denomination' => "0",
            'enabled' => "1",
        );
    }
    $games = collect($games_list);
    $games_list = $games->unique();

    return $games_list;
   }

   public function game_image($game_software, $game_raw, Request $request) {


       $url = 'http://wainwrighted.herokuapp.com/https://d1sc13y7hrlskd.cloudfront.net/optimized_images/landscape/'.$game_software.'/'.$game_raw;

       $curl = curl_init($url);
       curl_setopt($curl, CURLOPT_URL, $url);
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

       $headers = array(
          "Accept-Language: en-ZA,en;q=0.9",
          "Cache-Control: max-age=0",
          "Connection: keep-alive",
          "If-Modified-Since: Mon, 04 Jul 2022 14:25:39 GMT",
          "If-None-Match: '4d25ca82044578eccfa02a15010b6fcd'",
          "Upgrade-Insecure-Requests: 1",
          "User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Mobile Safari/537.36",
       );
       curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
       //for debug only!
       curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
       curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

       $resp = curl_exec($curl);
       curl_close($curl);
       header('Content-Type: image/webp; charset=UTF-8', true);

       $resp = str_replace("php","",$resp);
       $resp = str_replace("<?php","",$resp);
       $resp = str_replace("?>","",$resp);
       $resp = str_replace("<?=","",$resp);

       echo $resp;
   }

   public function gameslist_wainwright($games)
   {
    foreach($games as $game) {
        $explode_game = explode(':', $game->slug);
        if(isset($explode_game[1])) {
            $game_software = $explode_game[0];
            $game_raw = $explode_game[1];
            //$img_url = 'https://win.radio.fm/api/image/'.$game_software.'/'.$game_raw.'.webp';
            $img_url = 'https://wainwrighted.herokuapp.com/https://d1sc13y7hrlskd.cloudfront.net/optimized_images/landscape/'.$game_software.'/'.$game_raw.'.webp';
            if($game->provider === 'netent') {
                $img_url = 'https://wainwrighted.herokuapp.com/https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
            }
            if($game->provider === 'playngo') {
                $img_url = 'https://wainwrighted.herokuapp.com/https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
            }

            //$img_url = 'https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
        } else {
            $img_url = 'https://wainwrighted.herokuapp.com/https://parimatch.co.tz/service-discovery/service/pm-casino/img/tr:n-slots_game_image_desktop/Casino/eva/games/'.$game->gid.'.png';

            //$img_url = 'http://kohengroup.com/thumbnail/i-9102777/'.$game->gid.'.png';
        }

        $tags = array($game->type);

        if($game->bonusbuy === 1) {
            array_push($tags, 'bonusbuy');
        }

        if($game->jackpot === 1) {
            array_push($tags, 'jackpot');
        }

        $games_list[] = array(
            'id' => $game->gid,
            'slug' => $game->slug, //slug used in api
            'name' => $game->name,
            'provider' => $game->provider,
            'tags' => $tags,
            'img' => $img_url, // link to image
            'status' => 'active',
            'created_at' => $game->created_at ?? now(),
            'updated_at' => $game->updated_at ?? now(),
        );
    }
    $games = collect($games_list);
    $games_list = $games->unique();

    return $games_list;
   }

public function providerslist_wainwright($providers, $count) {
    if($count < 2) {
        $games_count = OperatorGameslist::where('provider', $providers[0]['slug'])->count();
        $providerslist = array(
            'id' => $providers[0]['slug'],
            'slug' => $providers[0]['slug'],
            'name' => ucfirst($providers[0]['name']),
            'parent' => NULL,
            'eligible_games' => $games_count,
            'icon' => 'ResponsiveIcon',
            'provider' => $providers[0]['provider'],
            'created_at' => now(),
            'updated_at' => now(),
        );
    } else {
    foreach($providers as $provider) {
        $games_count = OperatorGameslist::where('provider', $provider['slug'])->count();
        $providerslist[] = array(
            'id' => $provider['slug'],
            'slug' => $provider['slug'],
            'name' => ucfirst($provider['slug']),
            'parent' => NULL,
            'eligible_games' => $games_count,
            'icon' => 'ResponsiveIcon',
            'provider' => $provider['slug'],
            'created_at' => now(),
            'updated_at' => now(),
        );
    }
    }


    return $providerslist;
}

public function game_descriptions() {
    $cache_length = 300; // 300 seconds = 5 minutes

    if($cache_length === 0) {
        $game_desc = file_get_contents(__DIR__.'../../game_descriptions.json');
    }
    $game_desc = Cache::remember('gameDescriptions', 300, function () {
        return file_get_contents(__DIR__.'/../../game_descriptions.json');
    });
    $g2 = json_decode($game_desc, true);

    return $g2;
}

public function tags() {

    $tags = [
    [
        'id' => 1,
        'name' => 'Slots',
        'slug' => 'slots',
        'icon' => null,
        'image' => [],
        'details' => 'Slotmachine Games',
        'type_id' => 3,
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null,
        'type' => null,
    ],
    [
        'id' => 2,
        'name' => 'Live',
        'slug' => 'live',
        'icon' => null,
        'image' => [],
        'details' => 'Live Games',
        'type_id' => 3,
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null,
        'type' => null,
    ],
    [
        'id' => 3,
        'name' => 'Jackpot',
        'slug' => 'jackpot',
        'icon' => null,
        'image' => [],
        'details' => 'Jackpot Games',
        'type_id' => 3,
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null,
        'type' => null,
    ],
    [
        'id' => 4,
        'name' => 'Bonus-Buy',
        'slug' => 'bonusbuy',
        'icon' => null,
        'image' => [],
        'details' => 'Bonus Buy Game Feature',
        'type_id' => 3,
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null,
        'type' => null,
    ],
    ];

    return collect($tags)->paginate(100);
}

public function play($slug, Request $request)
{
    if($request->ip() === '127.0.0.1') {
        return 'localhost - load /play/localhost/{slug} instead';
    }

    $ip = $request->ip();
    $format_time_to_hour = Carbon\Carbon::parse(now())->format('H');
    $format_time_to_day = Carbon\Carbon::parse(now())->format('d');
    $player_id = md5($ip.$format_time_to_hour.$format_time_to_day);

    $create_session_apiurl = config('casino-dog-operator-api.endpoints.create_session')."?game=".$slug."&operator_key=".$this->operator_key."&player=".$player_id."&currency=USD&mode=real";
    $create_session_request = Http::get($create_session_apiurl);
    if(isset($create_session_request['message'])) {
    	if(isset($create_session_request['message']['session_url'])) {
	return redirect($create_session_request['message']['session_url']);
	} else {
	return $create_session_request['message'];
	}
    } else {
	return 'error'.$create_session_request;
    }
}

public function games_info($slug, Request $request)
{
    $http = $this->game_info($slug);
    /* Generate a user ID based on IP that resets every hour */
    $ip = $request->ip();
    $format_time_to_hour = Carbon\Carbon::parse(now())->format('H');
    $format_time_to_day = Carbon\Carbon::parse(now())->format('d');
    $player_id = md5($ip.$format_time_to_hour.$format_time_to_day);

    $create_session = env('APP_URL')."/api/play/".$http['slug'];
    $cached_session = Cache::get($player_id.$http['slug']);
    if($cached_session) {
        return $cached_session;
    }

    $orig_tags = $http['tags'];
    $tags = [];
    if(str_contains(json_encode($orig_tags), 'slots')) {
        $data = [
            'id' => 1,
            'name' => 'Slots',
            'slug' => 'slots',
            'icon' => null,
            'image' => [],
            'details' => 'Slotmachine Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }
    if(str_contains(json_encode($orig_tags), 'live')) {
        $data = [
            'id' => 2,
            'name' => 'Live',
            'slug' => 'live',
            'icon' => null,
            'image' => [],
            'details' => 'Live Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'bonusbuy')) {
        $data = [
            'id' => 3,
            'name' => 'Bonus Buy',
            'slug' => 'bonusbuy',
            'icon' => null,
            'image' => [],
            'details' => 'Bonus Buy Feature',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'jackpot')) {
        $data = [
            'id' => 4,
            'name' => 'jackpot',
            'slug' => 'Jackpot',
            'icon' => null,
            'image' => [],
            'details' => 'Jackpot',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'casino')) {
        $data = [
            'id' => 4,
            'name' => 'Casino Table Game',
            'slug' => 'casino',
            'icon' => null,
            'image' => [],
            'details' => 'Casino Table Game',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'live')) {
        $data = [
            'id' => 1,
            'name' => 'Live',
            'slug' => 'live',
            'icon' => null,
            'image' => [],
            'details' => 'Live Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    $data = [
        'id' => $http['id'],
        'name' => $http['name'],
        'slug' => $http['slug'],
        'player_id' => $player_id,
        'real_url' => $create_session,
        'description' => $http['description'],
        'play_real' =>  $create_session,
        'fake_iframe_url' => 'https://google.com',
        'status' => 'active',
        'image' => $http['image'],
        'tags' =>  $tags,
        'type' => $http['type'],
        'provider' => [
            'name' => $http['provider'],
            'slug' => $http['provider'],
            'cover_image' => [
                'original' => $http['image'],
                'thumbnail' => $http['image'],
            ],
            'logo' => [
                'original' => $http['image'],
                'thumbnail' => $http['image'],
            ],
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ],
    ];
    Cache::set($player_id.$http['slug'], $data, 90);

    return response()->json($data);
}


public function game_info($slug)
{
    $select_game = OperatorGameslist::where('slug', $slug)->first();

    if(!$select_game) {
        abort(400, 'Game not found');
    }
    $desc = collect($this->game_descriptions());

    $game_description = $desc->where('identifier', $select_game->gid)->first();
    if(!$game_description) {
        $game_desc = '';
    } else {
        $game_desc = $game_description['description'];
    }

    $tags = array($select_game->type);

    if($select_game->bonusbuy === 1) {
        array_push($tags, 'bonusbuy');
    }

    if($select_game->jackpot === 1) {
        array_push($tags, 'jackpot');
    }

    $explode_game = explode(':', $select_game->slug);
    if(isset($explode_game[1])) {
        $game_software = $explode_game[0];
        $game_raw = $explode_game[1];
        $img_url = 'https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
    } else {
        $img_url = 'http://kohengroup.com/thumbnail/i-9102777/'.$select_game->gid.'.png';
    }

    $game_info = array(
        'id' => $select_game->gid,
        'slug' => $select_game->slug,
        'name' => $select_game->name,
        'description' => $game_desc,
        'provider' => $select_game->provider,
        'status' => "active",
        'image' => $img_url,
        'tags' => $tags,
        'type' => $select_game  ->type,
    );

    return $game_info;
}

   public function gameslist_trimmed($games)
   {
    foreach($games as $game) {
        $explode_game = explode(':', $game->slug);
        if(isset($explode_game[1])) {
        $game_software = $explode_game[0];
        $game_raw = $explode_game[1];
        }
        $games_list[] = array(
            'id' => $game->gid,
            'api' => $game->slug, //slug used in api
            'n' => $game->name, // game name
            'bb' => $game->bonusbuy, // bonusbuy
            'jp' => $game->jackpot,
            'p' => $game->provider, // game provider
            'dp' => $game->demoplay, //demoplay
            't' => $game->type, //game type
            'img' => 'https://cdn.softswiss.net/i/s3/'.$game_software.'/'.$game_raw.'.png', // link to image
            'pop' => $game->popularity, // popularity game (lower is higher place on list)
            'on' => $game->enabled, //if game is enabled
        );
    }

    $schema_layout = array(
            'id' => 'game_id',
            'api' => 'game_slug',
            'n' => 'game_name',
            'p' => 'game_provider',
            't' => 'game_type',
            'img' => 'game_image',
            'pop' => 'game_popularity',
            'bb' => 'game_feature_bonusbuy',
            'jp' => 'game_feature_jackpot',
            'dp' => 'game_feature_demoplay',
            'on' => 'game_enabled',
    );

    return $games_list;
   }

   public function games(Request $request)
   {
	return $this->gamesListEndpoint('wainwright_casino', $request);

   }

   public function gamesListEndpoint(string $layout, Request $request)
   {
        $cache_length = 60;
        $limit = 20;
        if($request->limit) {
            if(is_numeric($request->limit)) {
                if($request->limit > 0) {
                    if($request->limit > 100) {
                        $limit = (int) 100;
                    } else {
                    $limit = (int) $request->limit;
                    }
                }
            }
        }

        if($cache_length === 0) {
            return OperatorGameslist::all()->sortBy('popularity');
        }
        $games = Cache::remember('getGamesList', 120, function () {
            return OperatorGameslist::all()->sortBy('popularity');
        });

       $bonus = 0;
       $jackpot = 0;

       if($request->searchJoin) {
           if(str_contains('tags.slug:bonusbuy', $request->searchJoin)) {
              $games = $games->where('bonusbuy', '=', 1)->all();
           }
           if(str_contains('tags.slug:jackpot', $request->searchJoin)) {
              $games = $games->where('jackpot', '=', 1)->all();
           }
        }


       if($request->bonus) {
          if($request->bonus === '1') {
              $games = $games->where('bonusbuy', '=', 1)->all();
          }
       }
        if($request->jackpot) {
           if($request->jackpot === '1') {
               $games = $games->where('jackpot', '=', 1)->all();
           }
        }


        if($request->search) {
          $provider_explode = explode('categories.slug:', $request->search);
          if(isset($provider_explode[1])) {
              $exploded = explode(';', $provider_explode[1]);
              $games = $games->where('provider', $exploded[0])->all();
          }
        }

        if($request->provider) {
            $games = $games->where('provider', $request->provider)->all();
        }

        $showAll = 0;
        if($request->showAll) {
            if($request->showAll === 'true') {
            $showAll = 1;
            }
        }

        if($layout === 'ocb') {
            $games = $this->gameslist_ocb($games);
        }

        if($layout === 'trimmed') {
            $games = $this->gameslist_trimmed($games);
        }

        if($layout === 'wainwright_casino') {
            $games = $this->gameslist_wainwright($games);
        }

        return collect($games)->paginate($limit);
    }


   public function categories(Request $request)
   {
	return $this->providersListEndpoint($request);
   }

   public function providersListEndpoint(Request $request)
   {
    $cache_length = 60;
    $limit = 25;
    if($request->limit) {
        if(is_numeric($request->limit)) {
            if($request->limit > 0) {
                if($request->limit > 100) {
                    $limit = (int) 100;
                } else {
                $limit = (int) $request->limit;
                }
            }
        }
    }

    $providers = collect(OperatorGameslist::providers());
    return collect($this->providerslist_wainwright($providers, $limit))->paginate($limit);

   }



   public function createSessionEndpoint(Request $request)
    {
        $validate = $this->createSessionValidation($request);
        if($validate->status() !== 200) {
            return $validate;
        }
        $data = [
            'game' => $request->game,
            'currency' => $request->currency,
            'player' => $request->player,
            'operator_key' => $request->operator_key,
            'mode' => $request->mode,
            'request_ip' => CasinoDog::requestIP($request),
        ];


        $session_create = SessionsHandler::createSession($data);
        if($session_create['status'] === 'success') {
            return response()->json($session_create, 200);
        } else {
            return $this->respondError($session_create);
        }
    }

    public function createSessionValidation(Request $request) {
        $validator = Validator::make($request->all(), [
            'game' => ['required', 'max:65', 'min:3'],
            'player' => ['required', 'min:3', 'max:100', 'regex:/^[^(\|\]`!%^&=};:?><’)]*$/'],
            'currency' => ['required', 'min:2', 'max:7'],
            'operator_key' => ['required', 'min:10', 'max:50'],
            'mode' => ['required', 'min:2', 'max:15'],
        ]);

        if ($validator->stopOnFirstFailure()->fails()) {
            $errorReason = $validator->errors()->first();
            $prepareResponse = array('message' => $errorReason, 'request_ip' => CasinoDog::requestIP($request));
            return $this->respondError($prepareResponse);
        }

        $operator_verify = OperatorsController::verifyKey($request->operator_key, CasinoDog::requestIP($request));
        if($operator_verify === false) {
                $prepareResponse = array('message' => 'Operator key did not pass validation.', 'request_ip' => CasinoDog::requestIP($request));
                return $this->respondError($prepareResponse);
        }

        $operator_ping = OperatorsController::operatorPing($request->operator_key, CasinoDog::requestIP($request));
        if($operator_ping === false) {
            $prepareResponse = array('message' => 'Operator ping failed on callback.', 'request_ip' => CasinoDog::requestIP($request));
            return $this->respondError($prepareResponse);
        }

        if($request->mode !== 'real') {
            $prepareResponse = array('message' => 'Mode can only be \'demo\' or \'real\'.', 'request_ip' => CasinoDog::requestIP($request));
            return $this->respondError($prepareResponse);
        }
        return $this->respondOk();
    }
}
